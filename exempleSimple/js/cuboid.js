const yellowRectangle = document.getElementById("yellowRectangle");
let cuboid = {
  length: 2, 
  width : 3,
  height: 4,
  setVolume: function (){ 
    const areaElement = document.getElementById("volume");
    areaElement.textContent = this.length * this.width * this.height;
  },
  incrLength: function (){
    this.length++;
    document.getElementById("cuboidLength").textContent = this.length;
    this.setVolume();
  },
  decrLength: function (){
    this.length--;
    document.getElementById("cuboidLength").textContent = this.length;
    this.setVolume();
  },
  incrWidth: function (){
    this.width++;
    document.getElementById("cuboidWidth").textContent = this.width;
    yellowRectangle.style.width=10*this.width+"px";
    this.setVolume();
  },
  decrWidth: function (){
    this.width--;
    document.getElementById("cuboidWidth").textContent = this.width;
    yellowRectangle.style.width=10*this.width+"px";
    this.setVolume();
  },
  incrHeight: function (){
    this.height++;
    document.getElementById("cuboidHeight").textContent = this.height;
    yellowRectangle.style.height=10*this.height+"px";
    this.setVolume();
  },
  decrHeight: function (){
    this.height--;
    document.getElementById("cuboidHeight").textContent = this.height;
    yellowRectangle.style.height=10*this.height+"px";
    this.setVolume();
  },
};

